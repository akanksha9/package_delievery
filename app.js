var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var db = require('./db.js');
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var md5 = require('md5');
var session = require('express-session');
var tokenService = require('./app/services/tokenAuth');
var userTokenObj = require('./app/models/users/userTokens.js');



var adminLoginObj = require('./app/models/adminlogins/adminlogin.js');
var userObj = require('./app/models/users/users.js');
var packageObj = require('./app/models/packages/package.js');
var constantObj = require('./constants.js');
var travellerObj = require('./app/models/traveller/traveller.js');
var settingObj = require('./app/models/setting/setting.js');

var dealObj = require('./app/models/dealStatus/dealStatus.js');
var dealEditObj = require('./app/models/dealEdit/dealEdit.js');

var NodeGeocoder = require('node-geocoder');

var crypto = require('crypto');
var connect = require('connect');
var key = 'MySecretKey12345';
var iv = '1234567890123456';
var cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);


var app = express();
app.use(cors());
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json({
  limit: '100mb'
}));





passport.use('bearer', new BearerStrategy(function(token, done) {
  tokenService.verifyToken(token, function(e, s) {
    if (e) {
      return done(e);
    }
    console.log(token)
    userTokenObj.findOne({
        token: token
      })
      .populate('admin')
      .populate('user')
      .exec(function(err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false);
        }

        if (user.admin == undefined) {
          return done(null, user.user, {
            scope: 'all'
          });
        }
        return done(null, user.admin, {
          scope: 'all'
        }); 
      });
  });
}));



function findByUsername(username, fn) {
  for (var i = 0, len = users.length; i < len; i++) {
    var user = users[i];
    if (user.username === username) {
      return fn(null, user);
    }
  }
  return fn(null, null);
}

//admin login
var LocalStrategy = require('passport-local').Strategy;
passport.use('adminLogin', new LocalStrategy(
  function(username, password, done) {
    password = JSON.parse(JSON.stringify(password));
    password = md5(password);
    console.log("inside admin login")
    adminLoginObj.findOne({
      username: username
    }, function(err, adminuser) {

      if (err) {
        return done(err);
      }

      if (!adminuser) {
        console.log("in adminuser");
        return done(null, false);
      }

      if (adminuser.password != password) {
        return done(null, false);
      }
      //generate a token here and return 
      var authToken = tokenService.issueToken({
        sid: adminuser
      });
      // save token to db  ; 
      var tokenObj = new userTokenObj({
        "admin": adminuser._id,
        "token": authToken
      });

      tokenObj.save(function(e, s) {});
      
      return done(null, {
        id: adminuser._id,
        username: adminuser.username,
        firstname: adminuser.firstname,
        lastname: adminuser.lastname,
        token: authToken,
        image: adminuser.prof_image
      });

    });
  }
));
passport.serializeUser(adminLoginObj.serializeUser);
passport.deserializeUser(adminLoginObj.deserializeUser);




//userlogin
passport.use('userLogin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  },
  function(req, email, password, done) {
    console.log("reqhsadgjhADSD",req.body);
    var device_token=req.body.device_token;
    var device_type=req.body.device_type;
    userObj.findOne({
      email: email
    }, function(err, user) {
      console.log("FFFFFFFFFFFFFFFF",user)
      if (err) {
        console.log("errrr")
        return done(err, {
          "message": "Error"
        });
      }

      if (!user) {
        console.log("Invalid user")
        return done(null, {
          "message": "Invalid email"
        });
      }

      if (user.password != password) {
        console.log("invalid password")
        return done(null, {
          "message": "Invalid password"
        });
      }
      //returning specific data
      return done(null, {
        _id: user._id,
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        phone_no: user.phone_no,
        facebook_id: user.facebook_id,
        image: user.image,
        gender:user.gender,
        device_token: device_token,
        device_type: device_type,
        reqOnYourTraveller: user.reqOnYourTraveller,
       reqOnYourPackage: user.reqOnYourPackage,
       newTravellerNearBy: user.newTravellerNearBy,
       newPackageNearBy: user.newPackageNearBy,
       distanceSelected: user.distanceSelected,
       profileVisibility: user.profileVisibility,
       notificationOnTracking:user.notificationOnTracking
       

      });
    });
  }
));

passport.serializeUser(function(userLoginObj, done) {
  done(null, userLoginObj)
})
passport.deserializeUser(function(userLoginObj, done) {
  done(null, userLoginObj)
})



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());

app.use(session({
  secret: 'keyboard-cat',
  cookie: {
    secure: true
  },
  proxy: true,
  resave: true,
  saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));





require('./routes/adminlogin')(app, express, passport);
require('./routes/users')(app, express, passport);
require('./routes/package')(app, express, passport);
require('./routes/traveller')(app, express, passport);
require('./routes/eManagement')(app, express, passport);
require('./routes/common')(app, express, passport);
require('./routes/dealStatus')(app, express, passport);
require('./routes/setting')(app, express, passport);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers


if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}


app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;