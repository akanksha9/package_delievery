//var baseUrl = "http://52.39.212.226:4075";
var baseUrl = "http://172.24.5.36:4106";


var webservices = {

    "authenticate": baseUrl + "/adminlogin/authenticate",
    "logout": baseUrl + "/adminlogin/logout",
    "forgot_password": baseUrl + "/adminlogin/forgot_password",
    "adminResetPassword": baseUrl + "/adminlogin/resetPassword",
    "changePassword": baseUrl + "/adminlogin/changePassword",
    "findOneAdminInfo": baseUrl + "/adminlogin/adminInfo",
    "saveProfile": baseUrl + "/adminlogin/saveProfile",
    "uploadProImg": baseUrl + "/adminlogin/uploadProImg",
    "commissionSetting": baseUrl + "/adminlogin/commissionSetting",

    //user
    
    "userList": baseUrl + "/users/list",
    "update": baseUrl + "/users/update",
    "getCurrentUserData": baseUrl + "/users/getCurrentUserData",
    "unSubscribe": baseUrl + "/users/unSubscribe",
    "allUsersCount": baseUrl + "/users/allUsersCount",
    "exportUserList": baseUrl + "/users/exportFile",
    "deleteUser": baseUrl + "/users/deleteUser",
    "totalUser": baseUrl + "/users/totalUser",
    "deleteUser": baseUrl + "/users/deleteUser",
    "bulkUpdateUser": baseUrl + "/users/bulkUpdate",
    "forgotPost": baseUrl + "/users/forgotPost",
   

//traveller
    
    "travellerList": baseUrl + "/traveller/travellerList",
    "update": baseUrl + "/traveller/update",
    "getCurrentTravellerData": baseUrl + "/traveller/getCurrentTravellerData",
    "unSubscribe": baseUrl + "/traveller/unSubscribe",
    "allTravellerCount": baseUrl + "/traveller/allTravellerCount",
    "exportTravellerList": baseUrl + "/traveller/exportFile",
    "deleteTraveller": baseUrl + "/traveller/deleteTraveller",
    "totalTraveller": baseUrl + "/traveller/totalTraveller",
    "bulkUpdateTraveller": baseUrl + "/traveller/bulkUpdate",
    "forgotPost": baseUrl + "/traveller/forgotPost",
    
    


"totalDeliveries": baseUrl + "/dealStatus/totalDeliveries",
//package
    
    "packageList": baseUrl + "/package/list",
    "update": baseUrl + "/package/update",
    "getCurrentPackageData": baseUrl + "/package/getCurrentPackageData",
    "unSubscribe": baseUrl + "/package/unSubscribe",
    "allPackageCount": baseUrl + "/package/allPackageCount",
    "exportPackageList": baseUrl + "/package/exportFile",
    "deletePackage": baseUrl + "/package/deletePackage",
    "totalPackage": baseUrl + "/package/totalPackage",
    "deletePackage": baseUrl + "/package/deletePackage",
    "bulkUpdatePackage": baseUrl + "/package/bulkUpdate",
    "forgotPost": baseUrl + "/package/forgotPost",
    
    // //package

    // "addPackage": baseUrl + "/package/add",
    // "packages": baseUrl + "/package/list",
    // "getPackageDetail": baseUrl + "/package/getdetail",
    // "updatePackage": baseUrl + "/package/updatePackage",
    // "deletePackage": baseUrl + "/package/deletePackage",
    // "bulkUpdatePackage": baseUrl + "/package/bulkUpdate",



    // // help block 
    // "updateHelpBlock": baseUrl + "/help/updateHelpBlock",
    // "getHelpBlockListing": baseUrl + "/help/getHelpBlockListing",
    // "getHelpInformation": baseUrl + "/help/getHelpInformation",
    // "insertHelpInformation": baseUrl + "/help/insertHelpInformation",
    // "deleteHelp": baseUrl + "/help/deleteHelp",
   


    // // FAQ
    // "insertQuestionAnswer": baseUrl + "/FAQ/insertQuestionAnswer",
    // "allfaq": baseUrl + "/FAQ/allfaq",
    // "deleteAndQues": baseUrl + "/FAQ/deleteAndQues",



}
var nav = [{
        text: 'Dashboard',
        path: '/#/',
        icon: 'fa-dashboard',
        activeText: '/home'
    }, {
        text: 'Manage Users',
        path: '/#/users',
        icon: 'ion ion-person-add',
        activeText: '/users'
    }, {
        text: 'Manage Travellers',
        path: '/#/travellers',
        icon: 'ion ion-person-add',
        activeText: '/travellers'
    },
    {
        text: 'Manage Deliveries',
        path: '/#/',
        icon: 'fa-users',
        activeText: '/users'
    },

];

var facebookConstants = {
    "facebook_app_id": "1655859644662114"
}

var googleConstants = {

    "google_client_id": "54372597586-09u72notkj8g82vl3jt77h7cbutvr7ep.apps.googleusercontent.com",

}

var appConstants = {

    "authorizationKey": "dGF4aTphcHBsaWNhdGlvbg=="
}


var headerConstants = {

    "json": "application/json"

}

var pagingConstants = {
    "defaultPageSize": 10,
    "defaultPageNumber": 1
}

var messagesConstants = {

    //users
    "saveUser": "User saved successfully",
    "updateUser": "User updated successfully",
    "updateStatus": "Status updated successfully",
    "deleteUser": "User(s) deleted successfully",

    //questionnaires
    "saveQuestionnaire": "Questionnaire saved successfully",
    "updateQuestionnaire": "Questionnaire updated successfully",
    "deleteQuestionnaire": "Questionnaire deleted successfully",

    //questions
    "saveQuestion": "Question saved successfully",
    "updateQuestion": "Question updated successfully",
    "deleteQuestion": "Question deleted successfully",
    "updateStatus": "Question updated successfully",

    //Error
    "enterQuestion": "Please enter the question.",
    "selectAnswerType": "Please select the answer type.",
    "enterAnswer": "Please enter the answer.",
    "selectAnswerCorrect": "Please choose the answer as correct.",
    "enterKeyword": "Please enter the Keyword.",



}