'use strict'

angular.module('Travellers')

.factory('TravellerService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};



	service.totalTraveller = function(callback) {
		communicationService.resultViaGet(webservices.totalTraveller, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.getTraveller = function(travellerId, callback) {
		var serviceURL = webservices.findOneTraveller + "/" + travellerId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.getTravellerOne = function(travellerId, callback) {
		var serviceURL = webservices.findTraveller + "/" + travellerId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.getTravellerInfo = function(travellerId, callback) {
		var serviceURL = webservices.userJobsCount + "/" + travellerId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateTraveller = function(inputJsonString, travellerId, callback) {
		var serviceURL = webservices.update + "/" + travellerId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateTravellerStatus = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.bulkUpdateTraveller, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.approveTravellerStatus = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.approve, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getTravellerList = function(inputJsonString, callback) {
		console.log("aaaaaaaaaaaaaaaatttttttttttt")
		communicationService.resultViaPost(webservices.travellerList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);

		});

	}

	service.getCurrentTravellerData = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.getCurrentUserData, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateTravellerStatus = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.bulkUpdateTraveller, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.exportTravellerList = function(callback) {
		communicationService.resultViaGet(webservices.exportTravellerList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.updateTravellerdata = function(inputJsonString, callback) {
		communicationService.resultViaPost("/traveller/updateTravellerdata", appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateTravellerInformation = function(inputJsonString, callback) {

		console.log("inputJsonString", inputJsonString);

		$http.post("/traveller/updateTravellerInformation", inputJsonString, {

				headers: {
					'Content-Type': undefined
				}
			}).success(function(response) {
				if (response) {
					console.log("m here in resposnne")
					callback(response);

				} else {
					$q.reject(response);
					callback({
						response: $q.defer().promise
					});

				}
			})
			.error(function(err) {
				alert('There was some error uploading your files. Please try Uploading them again.');
			});
	}

	service.unSubscribe = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.unSubscribe, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.allTravellerCount = function(callback) {
		communicationService.resultViaGet(webservices.allTravellersCount, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.deleteTraveller = function(inputJsonString, callback) {
		communicationService.resultViaPost(webservices.deleteTraveller , appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	return service;


}]);