var userObj = require('./../../models/users/users.js');
var travellerObj = require('./../../models/traveller/traveller.js');
var dealStatus = require('./../../models/dealStatus/dealStatus.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');
var async = require('async');
var common = require('./../common/common.js');

let FCM = require('fcm-node');
let serverKey = 'AIzaSyBd2WIKJkE3PDe101B4hERHjVnjx46-D6E';
let fcm = new FCM(serverKey);


exports.addTravellerPlan = function(req, res) {
    var errorMessage = '';
    var messages = ''
        //console.log(req.body)
    var reqdata = req.body;

    //console.log("reqdata",reqdata)
    userObj.find({
        _id: req.body.user_id
    }, function(err, userdetail) {

        //console.log("userdetail",userdetail[0].first_name,userdetail[0].last_name);
        if (err) {
            console.log(err)
            outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': "error in user_id"
                },
                res.json(outputJSON);
        } else {
            if (userdetail == "") {
                outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': "user doesnot exists"
                    },
                    res.json(outputJSON);
            } else {
                //console.log("userdetail",userdetail)
                reqdata.traveller_name = userdetail[0].first_name + ' ' + userdetail[0].last_name;
                 reqdata.email = userdetail[0].email;
                 reqdata.image = userdetail[0].image;
                  reqdata.phone_no = userdetail[0].phone_no;
                //console.log("usersssssssssssssssssss1111111111111", userdetail[0].image)

                travellerObj(reqdata).save(reqdata, function(err, data) {
                    //console.log("image",reqdata);
                    if (err) {
                        //console.log("data",data)
                        outputJSON = {
                            'status': 'failure',
                            'messageId': 400,
                            'message': err
                        }
                        res.json(outputJSON);
                    } else {
                        outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': "Traveller Plan added successfully",
                                "data": {
                                    id: data._id
                                }
                            },
                           

                        userObj.findOne({
                            _id: data.user_id
                        }, function(err, result) {
                            if (result) {
                                console.log("result is", result)
                                if (result.device_type == 'android') {
                                    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera) 
                                        to: result.device_token,
                                        notification: {
                                            body: "New Traveller Plan has been added",
                                            title: "Push notification"
                                            
                                        }
                                    }
                                    console.log("message is", message)
                                    fcm.send(message, function(err, pushresponse) {
                                        if (err) {
                                            console.log("Something has gone wrong!", err);
                                            //cb(err,{"message":"error in push notification android"})
                                        } else {
                                            console.log("Successfully sent with response: ", pushresponse);
                                            //cb(null,{pushresponse})
                                        }
                                    });
                                } else {
                                    console.log("inside ios")
                                    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera) 
                                        to: result.device_token,
                                        notification: {
                                            body: " ios"
                                        },

                                        data: { //you can send only notification or only data(or include both) 
                                            my_key: 'PackageDeliveryApp',
                                            //my_another_key: 'my another value'
                                        }
                                    };

                                    fcm.send(message, function(err, pushresponse) {
                                        if (err) {
                                            console.log("Something has gone wrong!", err);
                                            //cb(err,{"message":"error in push notification ios"})
                                        } else {
                                            console.log("Successfully sent with response: ", pushresponse);
                                            //cb(null,{pushresponse})
                                        }
                                    });
                                }
                            } else {
                                console.log("result not found");
                            }

                        });
                         res.json(outputJSON);



                    }
                })

            }
        }
    })
}

exports.totalTraveller = function(req, res) {

    var outputJSON = "";
    travellerObj.count({
        is_deleted: false
    }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

exports.planList = function(req, res) {

    var outputJSON = {
        'status': 'failure',
        'messageId': 203,
        'message': constantObj.messages.errorRetreivingData
    };
    travellerObj.find({
        traveller_plan_id: req.body.traveller_plan_id,
        is_deleted: false
    }, function(err, data) {

        var page = req.body.page || 1,
            count = req.body.count || 1;
        var skipNo = (page - 1) * count;

        var sortdata = {};
        var sortkey = null;
        for (key in req.body.sort) {
            sortkey = key;
        }
        if (sortkey) {
            var sortquery = {};
            sortquery[sortkey ? sortkey : '_id'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
        }
        //console.log("-----------query-------", query);
        console.log("sortquery", sortquery);
        console.log("page", page);
        console.log("count", count);
        console.log("skipNo", skipNo)
        var query = {};
        var searchStr = req.body.search;
        if (req.body.search) {
            query.$or = [{
                budget: new RegExp(searchStr, 'i')
            }]
        }
        var list = {
            destination: 1,
            source: 1,
            traveller_name: 1,
            budget: 1,
            rating: 1,
            image: 1,
            startDate: 1,
            endDate: 1,
            count: 1,

        }
        query.is_deleted = false;
        console.log("-----------query-------", query);
        travellerObj.find(query, list, count).exec(function(err, data) {


            if (err) {
                res.json("Error: " + err);
            } else {
                travellerObj.aggregate([{
                    $lookup: {
                        from: "dealstatuses",
                        localField: "_id",
                        foreignField: "travel_plan_id",
                        as: "detail"
                    }
                }, {
                    $unwind: '$detail'
                }, {
                    $match: {
                        'detail.is_req_to_traveller': true
                    }
                }]).exec(function(err, data1) {

                    // console.log("========data=======hjkkj",data1.length)
                    for (var i = 0; i < data.length; i++) {
                        console.log("here", typeof(data[i]._id))
                        for (var j = 0; j < data1.length; j++) {

                            console.log("data[i]._id", data[i]._id)
                            console.log("data1[j]._id", data1[j]._id)
                            var first = data[i]._id.toString();
                            var second = data1[j]._id.toString();
                            console.log("here2222", typeof(data[j]._id), "check", first == second)
                            console.log("here2345", typeof(data1[j]._id), "check", first == second)
                            if (first == second) {

                                data[i].count = data1[j].detail.length

                            }
                        }
                    }
                    console.log("========data", data)
                    if (err) {
                        console.log("tttte", err)
                        outputJSON = {
                            'status': 'failure',
                            'messageId': 203,
                            'message': 'data not retrieved '
                        };
                    } else {
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': 'data retrieve from products',
                            'data': data

                        }
                    }
                    res.status(200).jsonp(outputJSON);
                })
            }
        })

    })
}

exports.myPlans = function(req, res) {
    var outputJSON = {};
    let query = {};

    query.user_id = req.body.user_id;
    if (req.body.previous == 4) {
        query.is_delivered = req.body.previous; // previous = 4
    } else {
        query.is_delivered = {
            $nin: 4
        };
    }
   // console.log(query);
    travellerObj.find(query).exec(function(error, travellerData) {
        if (error) {
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': 'No record Found'
            };
            res.status(200).jsonp(outputJSON);
        } else {
            var count=0;
            async.each(travellerData, function(row, callback) {
                let qry = {};
                //console.log("travellerData", row);


                qry.traveller_plan_id = row._id;

                qry.is_req_to_traveller = true;
                // console.log("qyr", qry);



                dealStatus.find(qry).populate("package_id").exec(function(error, deals) {

                   console.log("deals",deals.length)
                   count=count+deals.length

                    row["deals"] = [];
                    row["deals"] = deals;

                    callback();
                });
            }, function(error) {
                if (error) {
                    console.log("error is here", error);
                    return res.status(400).jsonx({
                        success: false,
                        error: error
                    });
                } else {
                     console.log("deals", count);
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        "data": travellerData,
                        "count" : count

                    };

                }
                res.jsonp(outputJSON);
            });
        }

    });
}

exports.explore = function(req, res) {

    var outputJSON = {
        'status': 'failure',
        'messageId': 203,
        'message': constantObj.messages.errorRetreivingData
    };

    let query = {};
    let long;
    let lat;
    let km;
    let radius;

    query.user_id = {
        $ne: req.body.user_id
    };

    if (req.body.long && req.body.lat && req.body.km) {

        long = parseFloat(req.body.long);
        lat = parseFloat(req.body.lat);
        km = parseFloat(req.body.km);
        radius = km / 6378.1;
        // 6378.1
        query.location = [];
        query.location = {
                $geoWithin: {
                    $centerSphere: [
                        [long, lat], radius
                    ]
                }
            }
            // [long,lat]  

    }

    travellerObj.find(query, function(err, data) {

        if (err) {
            console.log("err", err)
            res.json("Error: " + err);

        } else {

            if (data.length == 0) {
                //console.log("inside null", data)
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': "You have not added any plan yet",
                    'data': data
                };
                res.json(outputJSON);

            } else {
                console.log("data", data)
                outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.successRetreivingData,
                        "data": data
                    },
                    res.json(outputJSON);
            }

        }
    });
}

exports.travellerList = function(req, res) {
    var outputJSON = {
        'status': 'failure',
        'messageId': 203,
        'message': constantObj.messages.errorRetreivingData
    };
    travellerObj.find({
        is_deleted: false
    }, function(err, data) {

        var page = req.body.page || 1,
            count = req.body.count || 1;
        var skipNo = (page - 1) * count;

        var sortdata = {};
        var sortkey = null;
        for (key in req.body.sort) {
            sortkey = key;
        }
        if (sortkey) {
            var sortquery = {};
            sortquery[sortkey ? sortkey : '_id'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
        }
        var query = {};
        var searchStr = req.body.search;
        if (req.body.search) {
            query.$or = [{
                traveller_name: new RegExp(searchStr, 'i')
            }, {
                source: new RegExp(searchStr, 'i')
            }, {
                created_date: new RegExp(searchStr, 'i')
            }]
        }
        query.is_deleted = false;
        travellerObj.find(query).exec(function(err, data) {
            if (err) {
                res.json("Error: " + err);
            } else {
                //outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, "data":data }, 
                //res.json(outputJSON);

                var length = data.length;
                travellerObj.find(
                        query
                    ).skip(skipNo).limit(count).sort(sortquery)
                    .exec(function(err, data1) {
                        //console.log(data)
                        if (err) {
                            console.log("tttte", err)
                            outputJSON = {
                                'status': 'failure',
                                'messageId': 203,
                                'message': 'data not retrieved '
                            };
                        } else {
                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': 'data retrieve from products',
                                'data': data1,
                                'count': length
                            }
                        }
                        res.status(200).jsonp(outputJSON);
                    })
            }
        });

    });
}

exports.deleteTraveller = function(req, res) {
    if (req.body._id) {
        travellerObj.update({
            _id: req.body._id
        }, {
            $set: {
                is_deleted: true
            }
        }, function(err, updRes) {
            if (err) {
                console.log(err);
            } else {
                console.log("device id updated", updRes);
                outputJSON = {
                    'status': 'failure',
                    'messageId': 203,
                    'data': updRes,
                    'message': "Traveller has been deleted successfully"
                };
                res.jsonp(outputJSON);


            }

        })
    }
}

exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var roleLength = inputData.data.length;
    var bulk = travellerObj.collection.initializeUnorderedBulkOp();
    for (var i = 0; i < roleLength; i++) {
        var travellerData = inputData.data[i];
        var id = mongoose.Types.ObjectId(travellerData.id);
        bulk.find({
            _id: id
        }).update({
            $set: travellerData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.travellerStatusUpdateSuccess
        };
    });
    res.jsonp(outputJSON);
}

exports.tracking = function(req, res) {
    var pId = req.params.id;
    // is_delivered status 1,2,3,4
    travellerObj.update({
            _id: pId
        }, {
            $set: req.body
        },
        function(err, pData) {

            if (err) {
                var outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': "Error",
                }
            } else {

                var outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': "Successfully updated",
                }
                res.jsonp(outputJSON);
            };
        });
}

exports.updatePlans = function(req, res) {
    if (req.body) {
        travellerObj.findOne({
            _id: req.params.id
        }, function(err, data) {
            if (err) {
                outputJSON = {
                    'status': 'error',
                    'messageId': 400,
                    'message': "not a valid _id"
                };
                res.jsonp(outputJSON)
            } else {
                console.log("req.body", req.body)
                travellerObj.update({
                    _id: req.params.id
                }, {
                    $set: req.body
                }, function(err, updatedresponse) {
                    if (err) {
                        outputJSON = {
                            'status': 'error',
                            'messageId': 400,
                            'message': "not Updated"
                        };
                        res.jsonp(outputJSON)
                    } else {
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': "updated successfully",
                            "data": updatedresponse
                        };
                        res.jsonp(outputJSON)
                    }
                })
            }
        })
    }
}