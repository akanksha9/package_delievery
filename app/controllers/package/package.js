var userObj = require('./../../models/users/users.js');
var dealStatus = require('./../../models/dealStatus/dealStatus.js');
var packageObj = require('./../../models/packages/package.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');
var fs = require('fs');
var NodeGeocoder = require('node-geocoder');
var emailService = require('./../email/emailService.js');
var md5 = require('md5');
var async = require('async');
var common = require('../common/common.js');
var dealObj = require('./../../models/dealStatus/dealStatus.js');
var traveller = require('./../../models/traveller/traveller.js');
var apn = require("apn");

var FCM = require('fcm-node');
var serverKey = 'AIzaSyBd2WIKJkE3PDe101B4hERHjVnjx46-D6E';
var fcm = new FCM(serverKey);


exports.addpackage = function(req, res) {
    var outputJSON = {
        'status': 'failure',
        'messageId': 400,
        'message': constantObj.messages.errorAddingItems
    };

    if (req.body.image != "") {
        console.log("inside image")
        var reqdata = req.body;
        uploadSkillImg(reqdata, function(response) {
            console.log(response)
            userObj.find({
                _id: req.body.user_id
            }, function(err, userdetail) {
                if (err) {
                    console.log(err)
                    outputJSON = {
                            'status': 'failure',
                            'messageId': 400,
                            'message': "error in user_id"
                        },
                        res.json(outputJSON);
                } else {
                    if (userdetail == "") {
                        outputJSON = {
                                'status': 'failure',
                                'messageId': 400,
                                'message': "user doesnot exists"
                            },
                            res.json(outputJSON);
                    } else {
                        var reqdata = req.body
                        reqdata.user_name = userdetail[0].first_name + ' ' + userdetail[0].last_name;
                        reqdata.email = userdetail[0].email;
                        reqdata.phone_no = userdetail[0].phone_no;
                        console.log("usersssssssssssssssssss", userdetail[0].image)
                            //console.log("userdetail",userdetail);


                        packageObj.update({
                            _id: response._id
                        }, {
                            $set: reqdata
                        }, {
                            multi: true
                        }, function(err, data) {
                            if (err) {
                                res.json("Error: " + err);
                            } else {
                                console.log("data", data)
                                outputJSON = {
                                        'status': 'success',
                                        'messageId': 200,
                                        'message': "package added successfully",
                                        "data": data
                                    },
                                    res.json(outputJSON);
                            }
                        });
                    }
                }
            })
        });
    } else {
        console.log("without image")
        var reqdata = req.body

        userObj.find({
            _id: req.body.user_id
        }, function(err, userdetail) {
            if (err) {
                console.log(err)
                outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': "error in user_id"
                    },
                    res.json(outputJSON);
            } else {
                if (userdetail == "") {
                    outputJSON = {
                            'status': 'failure',
                            'messageId': 400,
                            'message': "user doesnot exists"
                        },
                        res.json(outputJSON);
                } else {
                    reqdata.user_name = userdetail[0].first_name + ' ' + userdetail[0].last_name;
                    reqdata.email = userdetail[0].email;
                    reqdata.phone_no = userdetail[0].phone_no;
                    console.log("usersssssssssssssssssss", userdetail[0].image)
                        //console.log("userdetail",userdetail);
                    packageObj(reqdata).save(reqdata, function(err, packageData) {

                        if (err) {

                            outputJSON = {
                                'status': 'failure',
                                'messageId': 400,
                                'message': err
                            }
                            res.json(outputJSON);
                        } else {
                            var userdata = {};
                            // common.notify(packageData.user_id, function(error, success) {
                            //console.log("userdetail",userdetail);

                            outputJSON = {
                                    'status': 'success',
                                    'messageId': 200,
                                    'message': "package added successfully",
                                    "data": packageData
                                }
                                //});

                            let query = {};
                            let long;
                            let lat;
                            let km;
                            let radius;

                            query.user_id = {
                                $ne: req.body.user_id
                            };

                            if (req.body.long && req.body.lat && req.body.km) {

                                long = parseFloat(req.body.long);
                                lat = parseFloat(req.body.lat);
                                km = parseFloat(req.body.km);
                                radius = km / 6378.1;
                                // 6378.1
                                query.location = [];
                                query.location = {
                                        $geoWithin: {
                                            $centerSphere: [
                                                [long, lat], radius
                                            ]
                                        }
                                    }
                                    // [long,lat] 
                            }

                            userObj.find({

                                is_deleted: false
                            }, function(err, result) {
                                
                                if (result) {
                                    for (var i = 0; i < result.length; i++) {
                                        if (result[i].device_type == 'android') {
                                            var message = {  
                                                to: result[i].device_token,
                                                notification: {
                                                    body: "New Package has been added",
                                                    title: "Push notification"
                                                }
                                            }
                                            console.log("message is", message)
                                            fcm.send(message, function(err, pushresponse) {
                                                if (err) {
                                                    console.log("Something has gone wrong!", err);
                                                } else {
                                                    console.log("Successfully sent with response: ", pushresponse);
                                                }
                                            });
                                        } else if (result[i].device_type == 'ios') {
                                            console.log("inside ios")
                                            var message = {  
                                                to: result[i].device_token,
                                                notification: {
                                                    body: " ios"
                                                },

                                                data: {  
                                                    my_key: 'PackageDeliveryApp',
                                                }
                                            };

                                            fcm.send(message, function(err, pushresponse) {
                                                if (err) {
                                                    console.log("Something has gone wrong!", err);
                                                } else {
                                                    console.log("Successfully sent with response: ", pushresponse);
                                                }
                                            });
                                        } else {
                                            console.log("No device type found")
                                        }
                                    }
                                } else {
                                    console.log("result not found");
                                }

                            });
                            res.json(outputJSON);
                        }
                    })
                }
            }
        })
    }
}

/* ~~~~~~~~~~~~~Uploading image ~~~~~~~~~~~~~~~~~~~~~~~*/

uploadSkillImg = function(data, callback) {
    console.log("inside function image");
    var photoname = Date.now() + ".jpeg";
    var imageName = __dirname + "/../../../public/assets/upload/packImg" + photoname;

    if (data.image.indexOf("base64,") != -1) {
        var Data = data.image.split('base64,');
        var ext = Data[0].split('/');
        var format = ext[1].replace(';', '');
        var photoname = Date.now() + "." + format;
        var imageName = __dirname + "/../../../public/images/upload/" + photoname;
        var base64Data = Data[1];
        var base64Data = base64Data;
    } else {
        var base64Data = data.image.base64;
    }
    if (base64Data != undefined) {
        fs.writeFile(imageName, base64Data, 'base64', function(err) {
            if (err) {
                callback("Failure Upload");
            } else {
                var updateField = {};
                updateField = {
                    'image': photoname
                };
                var package = new packageObj({
                    image: photoname
                });
                //console.log('package',data)
                package.save(function(err, data) {
                    if (err) {
                        console.log(err)
                    }
                    if (data) {
                        callback(data);
                    }
                });
                //callback("Failure Upload");
            }
        });
    } else {
        callback("wrongFormat");
    }
}

exports.explore = function(req, res) {

    var outputJSON = {
        'status': 'failure',
        'messageId': 203,
        'message': constantObj.messages.errorRetreivingData
    };


    let query = {};
    let long;
    let lat;
    let km;
    let radius;

    query.user_id = {
        $ne: req.body.user_id
    };

    if (req.body.long && req.body.lat && req.body.km) {

        long = parseFloat(req.body.long);
        lat = parseFloat(req.body.lat);
        km = parseFloat(req.body.km);
        radius = km / 6378.1;
        // 6378.1
        query.location = [];
        query.location = {
                $geoWithin: {
                    $centerSphere: [
                        [long, lat], radius
                    ]
                }
            }
            // [long,lat]  

    }

    //console.log( "long=",long +"lat= ",lat  +"km = ", km );
    //console.log( req.body );
    //console.log( query );

    packageObj.find(query, function(err, data) {


        if (err) {
            console.log("err", err)
            res.json("Error: " + err);
        } else {

            if (data.length == 0) {
                //console.log("inside null", data)
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': "You have not added any package yet",
                    'data': data
                };
                res.json(outputJSON);

            } else {
                //console.log("data", data)
                outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.successRetreivingData,
                        "data": data
                    },
                    res.json(outputJSON);
            }
        }

    });
}

exports.packageList = function(req, res) {
    var outputJSON = {
        'status': 'failure',
        'messageId': 203,
        'message': constantObj.messages.errorRetreivingData
    };
    packageObj.find({
            is_deleted: false
        },
        function(err, data) {
            var page = req.body.page || 1,
                count = req.body.count || 1;
            var skipNo = (page - 1) * count;

            var sortdata = {};
            var sortkey = null;
            for (key in req.body.sort) {
                sortkey = key;
            }
            if (sortkey) {
                var sortquery = {};
                sortquery[sortkey ? sortkey : '_id'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
            }
            //console.log("-----------query-------", query);
            console.log("sortquery", sortquery);
            console.log("page", page);
            console.log("count", count);
            console.log("skipNo", skipNo)
            var query = {};
            var searchStr = req.body.search;
            if (req.body.search) {
                query.$or = [{
                    budget: new RegExp(searchStr, 'i')
                }]
            }
            var list = {
                destination: 1,
                source: 1,
                package_name: 1,
                budget: 1,
                image: 1,
                rating: 1,
                height: 1,
                width: 1,
                length: 1,
                quantity: 1,
                weight: 1,

            }
            query.is_deleted = false;

            if (req.body.long && req.body.lat && req.body.km) {

                long = parseFloat(req.body.long);
                lat = parseFloat(req.body.lat);
                km = parseFloat(req.body.km);
                radius = km / 6378.1;
                // 6378.1
                query.location = [];
                query.location = {
                    $geoWithin: {
                        $centerSphere: [
                            [long, lat], radius
                        ]
                    }
                }

            }

            console.log("-----------query-------", query);
            packageObj.find(query, list).exec(function(err, data) {
                if (err) {
                    res.json("Error: " + err);
                } else {
                    packageObj.aggregate([{
                        $lookup: {
                            from: "dealstatuses",
                            localField: "_id",
                            foreignField: "package_id",
                            as: "detail"
                        }
                    }, {
                        $unwind: '$detail'
                    }, {
                        $match: {
                            'detail.is_req_to_package': true
                        }
                    }]).exec(function(err, data1) {
                        console.log("detailssssssss", data1.length);
                        //console.log("========data=======hjkkj",data1.length)
                        for (var i = 0; i < data.length; i++) {
                            console.log("here", typeof(data[i]._id))
                            for (var j = 0; j < data1.length; j++) {

                                console.log("data[i]._id", data[i]._id)
                                console.log("data1[j]._id", data1[j]._id)
                                var first = data[i]._id.toString();
                                var second = data1[j]._id.toString();
                                console.log("here2222", typeof(data[j]._id), "check", first == second)
                                console.log("here2345", typeof(data1[j]._id), "check", first == second)
                                if (first == second) {
                                    console.log("gasjgdjkagsdj", data1[j].length)
                                    data[i].count = data1[j].detail.length
                                }
                            }
                        }
                        console.log("========data", data)
                        if (err) {
                            console.log("tttte", err)
                            outputJSON = {
                                'status': 'failure',
                                'messageId': 203,
                                'message': 'data not retrieved '
                            };
                        } else {
                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': 'data retrieve from products',
                                'data': data

                            }
                        }
                        res.status(200).jsonp(outputJSON);
                    })
                }
            })
        });
}

exports.tracking = function(req, res) {
    var pId = req.params.id;
    var tId = req.params.traveller;
    // is_delivered status 1,2,3,4,5
    if (req.body.is_delivered == 1) {
        userObj.find({
            is_deleted: false
        }, function(err, result) {

            if (result) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i].device_type == 'android') {
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: "status1",
                                title: "Push notification"
                            }
                        }
                        console.log("message is", message)
                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);

                            } else {
                                console.log("Successfully sent with response: ", pushresponse);

                            }
                        });
                    } else if (result[i].device_type == 'ios') {
                        console.log("inside ios")
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: " ios"
                            },

                            data: {
                                my_key: 'PackageDeliveryApp',

                            }
                        };

                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);

                            } else {
                                console.log("Successfully sent with response: ", pushresponse);

                            }
                        });
                    } else {
                        console.log("device not defined")
                    }
                }
            } else {
                console.log("result not found");
            }

        });

    } else if (req.body.is_delivered == 2) {
        userObj.find({
            is_deleted: false
        }, function(err, result) {
            if (result) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i].device_type == 'android') {
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: "status2",
                                title: "Push notification"
                            }
                        }
                        console.log("message is", message)
                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);

                            } else {
                                console.log("Successfully sent with response: ", pushresponse);

                            }
                        });
                    } else if (result[i].device_type == 'ios') {
                        console.log("inside ios")
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: " ios"
                            },

                            data: {
                                my_key: 'PackageDeliveryApp',

                            }
                        };

                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);

                            } else {
                                console.log("Successfully sent with response: ", pushresponse);

                            }
                        });
                    } else {
                        console.log("device not defined")
                    }
                }
            } else {
                console.log("result not found");
            }

        });
    } else if (req.body.is_delivered == 3) {
        userObj.find({
            is_deleted: false
        }, function(err, result) {
            if (result) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i].device_type == 'android') {
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: "status3",
                                title: "Push notification"
                            }
                        }
                        console.log("message is", message)
                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);
                            } else {
                                console.log("Successfully sent with response: ", pushresponse);
                            }
                        });
                    } else if (result[i].device_type == 'ios') {
                        console.log("inside ios")
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: " ios"
                            },
                            data: {
                                my_key: 'PackageDeliveryApp',
                            }
                        };

                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);
                            } else {
                                console.log("Successfully sent with response: ", pushresponse);
                            }
                        });
                    } else {
                        console.log("device not defined")
                    }
                }
            } else {
                console.log("result not found");
            }
        });
    } else if (req.body.is_delivered == 4) {
        userObj.find({
            is_deleted: false
        }, function(err, result) {
            if (result) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i].device_type == 'android') {
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: "status 4",
                                title: "Push notification"
                            }
                        }
                        console.log("message is", message)
                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);
                            } else {
                                console.log("Successfully sent with response: ", pushresponse);
                            }
                        });
                    } else if (result[i].device_type == 'ios') {
                        console.log("inside ios")
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: " ios"
                            },
                            data: {
                                my_key: 'PackageDeliveryApp',
                            }
                        };
                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);
                            } else {
                                console.log("Successfully sent with response: ", pushresponse);
                            }
                        });
                    } else {
                        console.log("device not defined")
                    }
                }
            } else {
                console.log("result not found");
            }
        });
    } else {
        userObj.find({
            is_deleted: false
        }, function(err, result) {
            if (result) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i].device_type == 'android') {
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: "status5",
                                title: "Push notification"
                            }
                        }
                        console.log("message is", message)
                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);
                            } else {
                                console.log("Successfully sent with response: ", pushresponse);
                            }
                        });
                    } else if (result[i].device_type == 'ios') {
                        console.log("inside ios")
                        var message = {
                            to: result[i].device_token,
                            notification: {
                                body: " ios"
                            },
                            data: {
                                my_key: 'PackageDeliveryApp',

                            }
                        };
                        fcm.send(message, function(err, pushresponse) {
                            if (err) {
                                console.log("Something has gone wrong!", err);
                            } else {
                                console.log("Successfully sent with response: ", pushresponse);
                            }
                        });
                    } else {
                        console.log(" device not defined")
                    }
                }
            } else {
                console.log("result not found");
            }

        });

    }
    packageObj.update({
            _id: pId,

        }, {
            $set: req.body
        },
        function(err, pData) {

            traveller.update({
                _id: tId
            }, {
                $set: req.body
            }, function(err, tData) {


                if (err) {
                    var outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': "Error",
                    }
                } else {
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': "Successfully updated",
                    }
                    res.jsonp(outputJSON);
                };
            });
        });
}

exports.myPackages = function(req, res) {
    var outputJSON = {};
    let query = {};

    query.user_id = req.body.user_id;
    if (req.body.previous == 4) {
        query.is_delivered = req.body.previous; // previous = 4
    } else {
        query.is_delivered = {
            $nin: 4
        };
    }
    console.log(query);
    packageObj.find(query).exec(function(error, packageData) {
        if (error) {
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': 'No record Found'
            };
            res.status(200).jsonp(outputJSON);
        } else {
            console.log("packageData", packageData);
            async.each(packageData, function(row, callback) {
                let qry = {};
                qry.package_id = row._id;
                qry.is_req_to_package = true;

                dealStatus.find(qry).populate('traveller_plan_id').exec(function(error, deals) {

                    row["deals"] = [];
                    row["deals"] = deals;
                    callback();
                });
            }, function(error) {
                if (error) {
                    console.log("error is here", error);
                    return res.status(400).jsonx({
                        success: false,
                        error: error,
                    });
                } else {
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        "data": packageData
                    };

                }
                res.jsonp(outputJSON);
            });
        }

    });
}

exports.totalPackage = function(req, res) {
    var outputJSON = "";
    packageObj.count({
        is_deleted: false
    }, function(err, data) {
        console.log(data)
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

exports.packageList = function(req, res) {
    var outputJSON = {
        'status': 'failure',
        'messageId': 203,
        'message': constantObj.messages.errorRetreivingData
    };
    packageObj.find({
        is_deleted: false
    }, function(err, data) {

        var page = req.body.page || 1,
            count = req.body.count || 1;
        var skipNo = (page - 1) * count;

        var sortdata = {};
        var sortkey = null;
        for (key in req.body.sort) {
            sortkey = key;
        }
        if (sortkey) {
            var sortquery = {};
            sortquery[sortkey ? sortkey : '_id'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
        }
        var query = {};
        var searchStr = req.body.search;
        if (req.body.search) {
            query.$or = [{
                package_name: new RegExp(searchStr, 'i')
            }, {
                source: new RegExp(searchStr, 'i')
            }, {
                created_date: new RegExp(searchStr, 'i')
            }]
        }
        query.is_deleted = false;
        packageObj.find(query).exec(function(err, data) {
            if (err) {
                res.json("Error: " + err);
            } else {
                //outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, "data":data }, 
                //res.json(outputJSON);

                var length = data.length;
                packageObj.find(
                        query
                    ).skip(skipNo).limit(count).sort(sortquery)
                    .exec(function(err, data1) {
                        //console.log(data)
                        if (err) {
                            console.log("tttte", err)
                            outputJSON = {
                                'status': 'failure',
                                'messageId': 203,
                                'message': 'data not retrieved '
                            };
                        } else {
                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': 'data retrieve from products',
                                'data': data1,
                                'count': length
                            }
                        }
                        res.status(200).jsonp(outputJSON);
                    })
            }
        });

    });
}

exports.deletePackage = function(req, res) {
    if (req.body._id) {
        packageObj.update({
            _id: req.body._id
        }, {
            $set: {
                is_deleted: true
            }
        }, function(err, updRes) {
            if (err) {
                console.log(err);
            } else {
                console.log("device id updated", updRes);
                outputJSON = {
                    'status': 'failure',
                    'messageId': 203,
                    'data': updRes,
                    'message': "Package has been deleted successfully"
                };
                res.jsonp(outputJSON);


            }

        })
    }
}

exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var roleLength = inputData.data.length;
    var bulk = packageObj.collection.initializeUnorderedBulkOp();
    for (var i = 0; i < roleLength; i++) {
        var packageData = inputData.data[i];
        var id = mongoose.Types.ObjectId(packageData.id);
        bulk.find({
            _id: id
        }).update({
            $set: packageData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.packageStatusUpdateSuccess
        };
    });
    res.jsonp(outputJSON);
}

exports.updatePackage = function(req, res) {
    var reqdata = {};
    var outputJSON = {
        'status': 'failure',
        'messageId': 203,
        'message': constantObj.messages.errorUpdatingItems
    };

    var reqdata = req.body;


    packageObj.find({
        _id: req.params.id
    }, function(err, packdetail) {
        if (err) {
            console.log(err)
            outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': "error"
                },
                res.json(outputJSON);
        } else {
            if (packdetail.length > 0) {
                if (req.body.image != "") {
                    var photoname = Date.now() + ".png";
                    var imageName = __dirname + "/../../../public/images/upload/" + photoname;
                    if (reqdata.image.indexOf("base64,") != -1) {
                        var Data = reqdata.image.split('base64,');
                        var ext = Data[0].split('/');
                        var format = ext[1].replace(';', '');
                        var photoname = Date.now() + "." + format;
                        var imageName = __dirname + "/../../../public/images/upload/" + photoname;
                        var base64Data = Data[1];
                        var base64Data = base64Data;
                    } else {
                        var base64Data = reqdata.image.base64;
                    }
                    if (base64Data != undefined) {
                        fs.writeFile(imageName, base64Data, 'base64', function(err) {
                            if (err) {
                                outputJSON = {
                                        'status': 'failure',
                                        'messageId': 400,
                                        'message': "Failure upload"
                                    },
                                    res.json(outputJSON);
                            } else {
                                packageObj.update({
                                    _id: req.params.id
                                }, {
                                    $set: req.body
                                }, function(err, data) {

                                    if (err) {
                                        console.log("err", err)
                                        outputJSON = {
                                                'status': 'failure',
                                                'messageId': 400,
                                                'message': "Wrong id"
                                            },
                                            res.json(outputJSON);
                                    } else {

                                        outputJSON = {
                                                'status': 'success',
                                                'messageId': 200,
                                                'message': "updated successfully",
                                                data: data
                                            },
                                            res.json(outputJSON);
                                    }
                                });
                            }
                        }); //file write
                    } else {
                        outputJSON = {
                                'status': 'failure',
                                'messageId': 400,
                                'message': "Wrong format"
                            },
                            res.json(outputJSON);
                    }
                } else {
                    packageObj.update({
                        _id: req.params.id
                    }, {
                        $set: req.body
                    }, {
                        multi: true
                    }, function(err, data) {
                        if (err) {
                            console.log("error", err)
                            outputJSON = {
                                    'status': 'failure',
                                    'messageId': 400,
                                    'message': "Error"
                                },
                                res.json(outputJSON)
                        } else {
                            outputJSON = {
                                    'status': 'success',
                                    'messageId': 200,
                                    'message': "updates successfully",
                                    data: data
                                },
                                res.json(outputJSON);
                        }
                    })
                }
            } else {
                console.log("id doesnt exists", packdetail) // package detail != null
                outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': "Id does not exist"
                    },
                    res.json(outputJSON);
            }
        }
    })
}

exports.parcelPosted = function(req, res) {
    var outputJSON = "";
    packageObj.count({
        is_deleted: false,
        user_id: req.body.user_id
    }, function(err, data) {
        console.log(data)
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}