 var userObj = require('./../../models/users/users.js');
 var packageObj = require('./../../models/packages/package.js');
 var crypto = require('crypto');
 var md5 = require('md5');
 var device = require('./../../models/devices/devices.js')
 var mongoose = require('mongoose');
 var twilio = require('twilio');
 var nodemailer = require('nodemailer');
 var constantObj = require('./../../../constants.js');
 var accountSid = 'ACf8246b33d4d1342704dee12500c7aba2'; // Your Account SID from www.twilio.com/console
 var authToken = '53800350222c1f6974ce1617563aaaa5'; // Your Auth Token from www.twilio.com/console
 var client = new twilio(accountSid, authToken);
 var commonjs = require('./../commonFunction/common.js');
 var fs = require('fs');
 var settingObj = require('./../../models/setting/setting.js');

 exports.updateLatLong = function(req, res) {
     var outputJSON = {};
     userObj.update({
         _id: req.params.id
     }, {
         $set: req.body
     }, function(uErr, uData) {
         if (uErr) {
             outputJSON = {
                 status: 400,
                 message: uErr,
             }
             res.jsonp(outputJSON)
         } else {
             outputJSON = {
                 status: 200,
                 message: 'successfully updated',
             }
             res.jsonp(outputJSON)
         }
     });
 }

 exports.userlogin = function(req, res) {
     var data = res.req.user;
     console.log("data", data)
     if (req.body.loginType == 1) {
         if (data.message == 'Invalid email') {
             var outputJSON = {
                 'status': 'failure',
                 'messageId': 400,
                 'message': "Invalid email"
             };
             res.jsonp(outputJSON)
         } else if (data.message == 'Invalid password') {
             var outputJSON = {
                 'status': 'failure',
                 'messageId': 400,
                 'message': "Invalid password"
             };
             res.jsonp(outputJSON)
         } else if (data.message == "Error") {
             var outputJSON = {
                 'status': 'failure',
                 'messageId': 400,
                 'message': "Error occured,try again later"
             };
             res.jsonp(outputJSON)
         } else {
             var device_token = data.device_token;
             var device_type = data.device_type;
             console.log("token,type", data);
             userObj.update({
                 "_id": data._id
             }, {
                 $set: {
                     "device_token": device_token,
                     "device_type": device_type
                 }
             }, function(err, updated) {
                 if (err) {
                     var outputJSON = {
                         'status': 'failure',
                         'messageId': 400,
                         'message': "Error occured,try again later"
                     };
                     res.jsonp(outputJSON)

                 } else {
                     console.log("updated*********", updated)
                     var outputJSON = {
                         'status': 'success',
                         'messageId': 200,
                         'message': "login successfully",
                         "data": data
                     };
                     res.jsonp(outputJSON)

                 }

             })
         }
     }
 };

 exports.faceBookLogin = function(req, res) {
     if (req.body.loginType == 2) {
         if (!req.body.facebook_id) {
             res.jsonp({
                 'status': 'failure',
                 'messageId': 401,
                 'message': 'Facebook Authentication Failed.'
             });
         } else {
             var details = {};
             var photoname = req.body.image;

             var folder = "";
             var updateField = {
                 'image': photoname
             };
             var height = 125;
             var width = 125;

             var imagename = __dirname + "/../../../public/assets/upload/profileImg/" + folder + photoname;
             if (req.body.first_name) {
                 details.first_name = req.body.first_name;
             }
             if (req.body.last_name) {
                 details.last_name = req.body.last_name;
             }
             if (req.body.phone_no) {
                 details.phone_no = req.body.phone_no;
             }
             if (req.body.email) {
                 details.email = req.body.email;
             }
             if (req.body.image) {
                 details.image = req.body.image;
             }
             if (req.body.password) {
                 details.password = req.body.password;
             }
             if (req.body.username) {
                 details.user_name = req.body.username;
             }
             if (req.body.gender) {
                 details.gender = req.body.gender;
             }
             if (req.body.device_token) {
                 details.device_token = req.body.device_token;
             }
             if (req.body.device_type) {
                 details.device_type = req.body.device_type;
             }
             console.log("details", details);
             details.facebook_id = req.body.facebook_id;
             details.loginType = 2;
             userObj.findOne({
                 facebook_id: req.body.facebook_id
             }, function(err, user) {
                 if (err) {
                     console.log("err", err)
                     res.jsonp({
                         "status": 'faliure',
                         "messageId": "401",
                         "message": "Sorry, Problem to login with facebook."
                     });
                 } else {
                     if (user == null) {

                         userObj(details).save(req.body, function(err, adduser) {
                             if (err) {
                                 console.log("err", err)
                                 res.jsonp({
                                     'status': 'failure',
                                     'messageId': 401,
                                     'message': 'User is not found'
                                 });
                             } else {

                                 res.jsonp({
                                     'status': 'success',
                                     'messageId': 200,
                                     'message': 'User logged in successfully',
                                     "data": adduser
                                 });
                                 console.log("adduser", adduser);


                             }
                         })
                     } else {

                         userObj.update({
                             user,
                             facebook_id: req.body.facebook_id
                         }, {
                             $set: {
                                 details
                             }
                         }, function(err, updatedata) {
                             if (err) {
                                 console.log("err", err);
                             } else {
                                 if (updatedata) {
                                     console.log("details", details);
                                     res.jsonp({
                                         'status': 'success',
                                         'messageId': 200,
                                         'message': 'Facebook credentials already exists',
                                         "data": user
                                     });


                                 }
                             }
                         })


                     }
                 }
             })
         }

     }
 };

 exports.user = function(req, res, next, id) {
     userObj.load(id, function(err, user) {
         if (err) {
             res.jsonp(err);
         } else if (!user) {
             res.jsonp({
                 err: 'Failed to load role ' + id
             });
         } else {

             req.userData = user;
             //console.log(req.user);
             next();
         }
     });
 };

 exports.findOne = function(req, res) {
     if (!req.userData) {
         outputJSON = {
             'status': 'failure',
             'messageId': 203,
             'message': constantObj.messages.errorRetreivingData
         };
     } else {
         outputJSON = {
             'status': 'success',
             'messageId': 200,
             'message': constantObj.messages.successRetreivingData,
             'data': req.userData
         }
     }
     res.jsonp(outputJSON);
 };

 exports.add = function(req, res) {
         var errorMessage = "";
         var outputJSON = "";
         var userModelObj = {};
         var password = md5(req.body.password);

         userModelObj = req.body;
         userObj.findOne({
             email: req.body.email
         }, function(err, user) {
             if (err) {
                 outputJSON = {
                     'status': 'failure',
                     'messageId': 401,
                     'message': errorMessage
                 };
             } else {
                 if (user == null) {
                     var image = req.body.image;
                     delete req.body.image;
                     userObj(userModelObj).save(req.body, function(err, data) {
                         if (err) { //console.log(err);
                             switch (err.name) {
                                 case 'ValidationError':

                                     for (field in err.errors) {
                                         if (errorMessage == "") {
                                             errorMessage = err.errors[field].message;
                                         } else {
                                             errorMessage += ", " + err.errors[field].message;
                                         }
                                     } //for
                                     break;
                             } //switch

                         } //if
                         else {
                             reqdata = {};
                             reqdata._id = data._id;
                             if (image != undefined) {
                                 reqdata.image = image;

                                 uploadProImg(reqdata, function(responce) {
                                     outputJSON = {
                                         'status': 'success',
                                         'messageId': 200,
                                         'message': req.headers.lang == "chtrad" ? chtradconstantObj.messages.userSuccess : (req.headers.lang == "chsimp" ? chsimpconstantObj.messages.userSuccess : constantObj.messages.userSuccess),
                                         'data': data
                                     };
                                     res.jsonp(outputJSON);
                                 });
                             } else {

                                 outputJSON = {
                                     'status': 'success',
                                     'messageId': 200,
                                     'message': req.headers.lang == "chtrad" ? chtradconstantObj.messages.userSuccess : (req.headers.lang == "chsimp" ? chsimpconstantObj.messages.userSuccess : constantObj.messages.userSuccess),
                                     'data': data
                                 };
                                 res.jsonp(outputJSON);
                             }
                         }
                     });

                 }
             }
         });
     }
     /////UploadImageFunction///////////////
 uploadProImg = function(data, callback) {

     //console.log("data",data);
     var photoname = data._id + '_' + Date.now() + '.jpg';

     var folder = "";
     var updateField = {
         'profile_image': photoname
     };
     var height = 125;
     var width = 125;

     var imagename = __dirname + "/../../../public/assets/upload/profileImg/" + folder + photoname;
     if (data.image.indexOf("base64,") != -1) {
         var Data = data.image.split('base64,');
         var base64Data = Data[1];
         var saveData = {};

         saveData.images = [{
             name: photoname
         }];

         //console.log("asdfasdsa",saveData)

         fs.writeFile(imagename, base64Data, 'base64', function(err) {
             if (err) {
                 console.log(err);
                 callback("Failure Upload");


             } else {
                 console.log('data._id', data._id)
                 userObj.update({
                     '_id': data._id
                 }, {
                     $set: {
                         "image": photoname

                     }
                 }, function(err, res) {
                     console.log(res);
                     callback(saveData);
                 });
                 //callback('saveData');

             }
         });
     } else {
         callback("Image  not selected");
     }
 }

 exports.updateSetting = function(req, res) {
     console.log("req.body", req.body)
     var outputJSON = {};

     userObj.findOne({
         _id: req.params.id
     }, function(err, data) {
         if (err) {
             console.log(err)
         } else {
             userObj.findOneAndUpdate({
                     _id: req.params.id
                 }, {
                     $set: {
                         profileVisibility: req.body.profileVisibility,
                         distanceSelected: req.body.distanceSelected,
                         newPackageNearBy: req.body.newPackageNearBy,
                         newTravellerNearBy: req.body.newTravellerNearBy,
                         reqOnYourPackage: req.body.reqOnYourPackage,
                         reqOnYourTraveller: req.body.reqOnYourTraveller,
                         notificationOnTracking: req.body.notificationOnTracking
                     }
                 }, {
                     new: true
                 },
                 function(err, Updata) {
                     console.log("err", err);
                     console.log("data", data)
                     if (err) {
                         outputJSON = {
                             'status': 'failure',
                             'messageId': 400,
                             'message': "Error"
                         }
                         res.jsonp(outputJSON)
                     } else {
                         console.log("dataaaaaaaaaa", Updata)
                         outputJSON = {
                             'status': 'success',
                             'messageId': 200,
                             'message': "Successfully updated",
                             'data': Updata
                         }
                         res.jsonp(outputJSON)
                     }
                 });
         }
     });
 }



 exports.update_user_info = function(req, res) {
     userObj.find({
         _id: req.body._id
     }, function(err, user_found) {
         if (err) {
             console.log(err);
             outputJSON = {
                 'status': 'Failure',
                 'messageId': 400,
                 'message': "Error"
             };
             res.jsonp(outputJSON)
         } else {
             if (user_found == "") {
                 outputJSON = {
                     'status': 'success',
                     'messageId': 200,
                     'message': "Not a valid _id"
                 };
                 res.jsonp(outputJSON)
             } else {
                 var logintype = user_found[0].loginType
                 if (req.body.email == "") {
                     console.log("email not updated")
                     userObj.update({
                         _id: req.body._id
                     }, {
                         $set: {
                             first_name: req.body.first_name,
                             last_name: req.body.last_name,
                             phone_no: req.body.phone_no,
                             password: req.body.password,
                             image: req.body.image
                         }
                     }, function(err, data) {
                         if (err) {
                             console.log("err", err);
                             outputJSON = {
                                 'status': 'failure',
                                 'messageId': 401,
                                 'message': errorMessage
                             };
                         } else {
                             outputJSON = {
                                 'status': 'success',
                                 'messageId': 200,
                                 'message': "updated successfully",
                                 'data': data
                             };
                             res.jsonp(outputJSON);
                         }
                     });
                 } else {
                     if (logintype == 1) {
                         userObj.find({
                             email: req.body.email
                         }, function(err, useremail) {
                             if (err) {
                                 console.log(err);
                                 outputJSON = {
                                     'status': 'Failure',
                                     'messageId': 400,
                                     'message': "Error"
                                 };
                                 res.jsonp(outputJSON)
                             } else {
                                 if (useremail == "") {
                                     userObj.findOneAndUpdate({
                                         _id: req.body._id
                                     }, {
                                         $set: {
                                             first_name: req.body.first_name,
                                             last_name: req.body.last_name,
                                             email: req.body.email,
                                             phone_no: req.body.phone_no,
                                             gender: req.body.gender,
                                             image: req.body.image
                                         }
                                     }, {
                                         new: true
                                     }, function(err, updateddata) {
                                         if (err) {
                                             outputJSON = {
                                                 'status': 'failure',
                                                 'messageId': 401,
                                                 'message': errorMessage
                                             };
                                             res.jsonp(outputJSON);
                                         } else {
                                             outputJSON = {
                                                 'status': 'success',
                                                 'messageId': 200,
                                                 'message': "updated successfully",
                                                 'data': updateddata
                                             };
                                             res.jsonp(outputJSON);
                                         }
                                     });
                                 } // end of if email is unique
                                 else {
                                     outputJSON = {
                                         'status': 'failure',
                                         'messageId': 400,
                                         'message': "Email already exist in database, please enter another email id"

                                     };
                                     res.jsonp(outputJSON);
                                 }
                             }
                         })
                     } // end of if loginType==1
                     if (logintype == 2) {
                         userObj.findOneAndUpdate({
                             _id: req.body._id
                         }, {
                             $set: {
                                 first_name: req.body.first_name,
                                 last_name: req.body.last_name,
                                 email: req.body.email,
                                 phone_no: req.body.phone_no,
                                 gender: req.body.gender,
                                 image: req.body.image
                             }
                         }, {
                             new: true
                         }, function(err, data) {
                             if (err) {
                                 outputJSON = {
                                     'status': 'failure',
                                     'messageId': 401,
                                     'message': errorMessage
                                 };
                                 res.jsonp(outputJSON);
                             } else {
                                 outputJSON = {
                                     'status': 'success',
                                     'messageId': 200,
                                     'message': "updated successfully",
                                     'data': data
                                 };
                                 res.jsonp(outputJSON);
                             }

                         });
                     } //if login ==2
                 } //end of else    
             }
         }
     })
 }


 exports.resetPost = function(req, res) {

     console.log("reset ", req.body);

     var pswdd = JSON.parse(JSON.stringify(req.body.newpwd));
     password = md5(pswdd);


     userObj.update({
         email: commonjs.decrypt(req.body.email)
     }, {
         $set: {
             password: password,
             verifyStr: ""
         }
     }, function(err, data) {

         if (err) {

             outputJSON = {
                 'status': 'failure',
                 'messageId': 203,
                 'message': constantObj.messages.errorSendingForgotPasswordEmail
             };
         } else {
             if (data.nModified > 0) {
                 outputJSON = {
                     'status': 'success',
                     'messageId': 200,
                     'message': 'Password has been chaged successfully'
                 }
             } else {
                 outputJSON = {
                     'status': 'failure',
                     'messageId': 203,
                     'message': 'You have already changed your password.'
                 };
             }
         }
         res.jsonp(outputJSON);
     });
 }

 exports.forgotPost = function(req, res) {

     if (req.body.email) {
         var details = req.body.email;
         var detailsdata = {};


         userObj.findOne({
             email: details
         }, function(err, data) {
             console.log("inside findOne", JSON.stringify(data))
             if (data == null) {
                 var response = {
                     "status": 'faliure',
                     "messageId": 401,
                     "message": "Email does not exist."
                 };
                 res.status(401).json(response);
             } else {
                 var email_encrypt = commonjs.encrypt(data.email);
                 var generatedText = commonjs.makeid();
                 var resetUrl = "http://" + req.headers.host + "/#/" + "users/resetPost/" + data._id;
                 console.log(resetUrl)
                 console.log("aghsvfaghvfsJAXDVS", email_encrypt, "GENERTEAD ", generatedText, "resetUrl", resetUrl)

                 var transporter = nodemailer.createTransport({
                     service: 'gmail',
                     auth: {
                         user: 'bridgit871@gmail.com',
                         pass: 'bridgit8711'
                     }
                 });
                 var message = '<html><body style="background-color: #f2f2f2"><div style="width:90%; padding: 15px; background-color: #fff; box-shadow: 3px 3px #dddddd;"><div style="padding-top:10px; background-color: #f0f0f0; height: 100px"><h2>Bridgit<h2></div><div style="padding-top:10px">Hi,</div><div style="padding-top:30px">Your email has been used in a password reset request.</div><div style="padding-top:20px">If you did not initiate this request, then ignore this message.</div><div style="padding-top:20px">Copy the link below into your browser to reset your password.</div><div style="padding-top:30px"><a href="' + resetUrl + '">Reset Password</a></div><div style="padding-top:50px">Regards,<br>Bridgit</div></div></body></html>'

                 transporter.sendMail({
                     from: 'bridgit871@gmail.com',

                     to: details,

                     subject: 'Password for Bridgit App',
                     html: message
                 });


                 res.status(200).send({
                     'resetUrl': resetUrl,
                     "status": 'success',
                     "messageId": 200,
                     "message": "Mail sent successfully."
                 });
             }



         })
     } else {
         response = {
             "status": 'failure',
             "messageId": 401,
             "message": "Pass required fields."
         };
         res.status(401).json(response);
     }
 }

 exports.list = function(req, res) {
     var outputJSON = {
         'status': 'failure',
         'messageId': 203,
         'message': constantObj.messages.errorRetreivingData
     };
     userObj.find({
         is_deleted: false
     }, function(err, data) {

         var page = req.body.page || 1,
             count = req.body.count || 1;
         var skipNo = (page - 1) * count;

         var sortdata = {};
         var sortkey = null;
         for (key in req.body.sort) {
             sortkey = key;
         }
         if (sortkey) {
             var sortquery = {};
             sortquery[sortkey ? sortkey : '_id'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
         }
         var query = {};
         var searchStr = req.body.search;
         if (req.body.search) {
             query.$or = [{
                 first_name: new RegExp(searchStr, 'i')
             }, {
                 email: new RegExp(searchStr, 'i')
             }, {
                 phone_no: new RegExp(searchStr, 'i')
             }]
         }
         query.is_deleted = false;
         userObj.find(query).exec(function(err, data) {
             if (err) {
                 res.json("Error: " + err);
             } else {
                 //outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, "data":data }, 
                 //res.json(outputJSON);

                 var length = data.length;
                 userObj.find(
                         query
                     ).skip(skipNo).limit(count).sort(sortquery)
                     .exec(function(err, data1) {
                         //console.log(data)
                         if (err) {
                             console.log("tttte", err)
                             outputJSON = {
                                 'status': 'failure',
                                 'messageId': 203,
                                 'message': 'data not retrieved '
                             };
                         } else {
                             outputJSON = {
                                 'status': 'success',
                                 'messageId': 200,
                                 'message': 'data retrieve from products',
                                 'data': data1,
                                 'count': length
                             }
                         }
                         res.status(200).jsonp(outputJSON);
                     })
             }
         });

     });
 }

 exports.deleteUser = function(req, res) {
     if (req.body._id) {
         userObj.update({
             _id: req.body._id
         }, {
             $set: {
                 is_deleted: true
             }
         }, function(err, updRes) {
             if (err) {
                 console.log(err);
             } else {
                 console.log("device id updated", updRes);
                 outputJSON = {
                     'status': 'failure',
                     'messageId': 203,
                     'data': updRes,
                     'message': "Customer has been deleted successfully"
                 };
                 res.jsonp(outputJSON);


             }

         })
     }
 }

 exports.totalUser = function(req, res) {

     var outputJSON = "";
     userObj.count({
         is_deleted: false
     }, function(err, data) {
         if (err) {
             outputJSON = {
                 'status': 'failure',
                 'messageId': 203,
                 'message': constantObj.messages.errorRetreivingData
             };
         } else {
             outputJSON = {
                 'status': 'success',
                 'messageId': 200,
                 'message': constantObj.messages.successRetreivingData,
                 'data': data
             }
         }
         res.jsonp(outputJSON);
     });
 }

 exports.bulkUpdate = function(req, res) {
     var outputJSON = "";
     var inputData = req.body;
     var roleLength = inputData.data.length;
     var bulk = userObj.collection.initializeUnorderedBulkOp();
     for (var i = 0; i < roleLength; i++) {
         var userData = inputData.data[i];
         var id = mongoose.Types.ObjectId(userData.id);
         bulk.find({
             _id: id
         }).update({
             $set: userData
         });
     }
     bulk.execute(function(data) {
         outputJSON = {
             'status': 'success',
             'messageId': 200,
             'message': constantObj.messages.userStatusUpdateSuccess
         };
     });
     res.jsonp(outputJSON);
 }