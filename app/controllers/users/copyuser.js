var userObj = require('./../../models/users/users.js');
var device = require('./../../models/devices/devices.js')
var mongoose = require('mongoose');
var twilio = require('twilio');
var nodemailer = require('nodemailer');
var constantObj = require('./../../../constants.js');
var accountSid = 'ACf8246b33d4d1342704dee12500c7aba2'; // Your Account SID from www.twilio.com/console
var authToken = '53800350222c1f6974ce1617563aaaa5'; // Your Auth Token from www.twilio.com/console
var client = new twilio(accountSid, authToken);
var commonjs = require('./../commonFunction/common.js');


exports.userlogin = function(req, res) {
    var data = res.req.user;
    console.log("data", data)
    if (req.body.loginType == 1) {
        if (data.message == 'Invalid email') {
            var outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': "Invalid email"
            };
            res.jsonp(outputJSON)
        } else if (data.message == 'Invalid password') {
            var outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': "Invalid password"
            };
            res.jsonp(outputJSON)
        } else if (data.message == "Error") {
            var outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': "Error occured,try again later"
            };
            res.jsonp(outputJSON)
        } else {
            var outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': "login successfully",
                "data": data
            };
            res.jsonp(outputJSON)
        }


    }
};

exports.faceBookLogin = function(req, res) {

    if (req.body.loginType == 2) {
        if (!req.body.facebook_id) {
            res.jsonp({
                'status': 'failure',
                'messageId': 401,
                'message': 'Facebook Authentication Failed.'
            });
        } else {
            var details = {};
            if (req.body.first_name) {
                details.first_name = req.body.first_name;
            }
            if (req.body.last_name) {
                details.last_name = req.body.last_name;
            }
            if (req.body.phone_no) {
                details.phone_no = req.body.phone_no;
            }
            if (req.body.email) {
                details.email = req.body.email;
            }
            if (req.body.password) {
                details.password = req.body.password;
            }
            if (req.body.username) {
                details.user_name = req.body.username;
            }
            details.facebook_id = req.body.facebook_id;
            details.loginType = 2;
            userObj.findOne({
                facebook_id: req.body.facebook_id
            }, function(err, user) {
                if (err) {
                    console.log("err", err)
                    res.jsonp({
                        "status": 'faliure',
                        "messageId": "401",
                        "message": "Sorry, Problem to login with facebook."
                    });
                } else {
                    if (user == null) {
                        userObj(details).save(req.body, function(err, adduser) {
                            if (err) {
                                console.log("err", err)
                                res.jsonp({
                                    'status': 'failure',
                                    'messageId': 401,
                                    'message': 'User is not found'
                                });
                            } else {
                                res.jsonp({
                                    'status': 'success',
                                    'messageId': 200,
                                    'message': 'User logged in successfully',
                                    "data": adduser
                                });
                                console.log("adduser", adduser);


                            }
                        })
                    } else {
                        userObj.update({
                            user,
                            facebook_id: req.body.facebook_id
                        }, {
                            $set: {
                                details
                            }
                        }, function(err, updatedata) {
                            if (err) {

                            } else {
                                if (updatedata) {
                                    res.jsonp({
                                        'status': 'success',
                                        'messageId': 200,
                                        'message': 'Facebook credentials already exists',
                                        "data": user
                                    });


                                }
                            }
                        })


                    }
                }
            })
        }

    }
};


exports.user = function(req, res, next, id) {
    userObj.load(id, function(err, user) {
        if (err) {
            res.jsonp(err);
        } else if (!user) {
            res.jsonp({
                err: 'Failed to load role ' + id
            });
        } else {

            req.userData = user;
            //console.log(req.user);
            next();
        }
    });
};


exports.findOne = function(req, res) {
    if (!req.userData) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.userData
        }
    }
    res.jsonp(outputJSON);
};


exports.add = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var userModelObj = {};
    userModelObj = req.body;
    userObj.findOne({
        email: req.body.email
    }, function(err, user) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } else {
            if (user == null) {
                userObj(userModelObj).save(req.body, function(err, data) {

                    if (err) {
                        switch (err.name) {
                            case 'ValidationError':

                                for (field in err.errors) {
                                    if (errorMessage == "") {
                                        errorMessage = err.errors[field].message;
                                    } else {
                                        errorMessage += ", " + err.errors[field].message;
                                    }
                                } //for
                                break;
                        } //switch

                        outputJSON = {
                            'status': 'failure',
                            'messageId': 401,
                            'message': errorMessage
                        };
                    } //if
                    else {
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': constantObj.messages.userSuccess,
                            'data': {
                                id: data._id
                            }
                        };
                    }
                    res.jsonp(outputJSON);
                });
            } else {
                console.log("inside else")
                outputJSON = {
                    'status': 'failure',
                    'messageId': 401,
                    'message': "user already exists"
                };
                res.jsonp(outputJSON);
            }
        }
    })
};


exports.update_user_info = function(req, res) {
    userObj.find({
        _id: req.body._id
    }, function(err, user_found) {
        if (err) {
            console.log(err);
            outputJSON = {
                'status': 'Failure',
                'messageId': 400,
                'message': "Error"
            };
            res.jsonp(outputJSON)
        } else {
            if (user_found == "") {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': "Not a valid _id"
                };
                res.jsonp(outputJSON)
            } else {
                var logintype = user_found[0].loginType
                if (req.body.email == "") {
                    console.log("email not updated")
                    userObj.update({
                        _id: req.body._id
                    }, {
                        $set: {
                            first_name: req.body.first_name,
                            last_name: req.body.last_name,
                            phone_no: req.body.phone_no,
                            password: req.body.password
                        }
                    }, function(err, data) {
                        if (err) {
                            console.log("err", err);
                            outputJSON = {
                                'status': 'failure',
                                'messageId': 401,
                                'message': errorMessage
                            };
                            res.jsonp(outputJSON);
                        } else {
                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': "updated successfully",
                                'data': data
                            };
                            res.jsonp(outputJSON);
                        }
                    });
                } else {
                    if (logintype == 1) {
                        userObj.findOne({
                            email: req.body.email
                        }, function(err, useremail) {
                            if (err) {
                                console.log(err);
                                outputJSON = {
                                    'status': 'Failure',
                                    'messageId': 400,
                                    'message': "Error"
                                };
                                res.jsonp(outputJSON)
                            } else {
                                if (useremail == "") {
                                    userObj.update({
                                        _id: req.body._id
                                    }, {
                                        $set: {
                                            first_name: req.body.first_name,
                                            last_name: req.body.last_name,
                                            email: req.body.email,
                                            password: req.body.password,
                                            phone_no: req.body.phone_no
                                        }
                                    }, function(err, updateddata) {
                                        if (err) {
                                            outputJSON = {
                                                'status': 'failure',
                                                'messageId': 401,
                                                'message': errorMessage
                                            };
                                            res.jsonp(outputJSON);
                                        } else {
                                            outputJSON = {
                                                'status': 'success',
                                                'messageId': 200,
                                                'message': "updated successfully",
                                                'data': updateddata
                                            };
                                            res.jsonp(outputJSON);
                                        }
                                    });
                                } // end of if email is unique
                                else {
                                    outputJSON = {
                                        'status': 'failure',
                                        'messageId': 400,
                                        'message': "Email already exist in database, please enter another email id"

                                    };
                                    res.jsonp(outputJSON);
                                }
                            }
                        })
                    } // end of if loginType==1
                    if (logintype == 2) {
                        userObj.update({
                            _id: req.body._id
                        }, {
                            $set: {
                                first_name: req.body.first_name,
                                last_name: req.body.last_name,
                                email: req.body.email,
                                phone_no: req.body.phone_no
                            }
                        }, function(err, data) {
                            if (err) {
                                outputJSON = {
                                    'status': 'failure',
                                    'messageId': 401,
                                    'message': errorMessage
                                };
                                res.jsonp(outputJSON);
                            } else {
                                outputJSON = {
                                    'status': 'success',
                                    'messageId': 200,
                                    'message': "updated successfully",
                                    'data': data
                                };
                                res.jsonp(outputJSON);
                            }

                        });
                    } //if login ==2
                } //end of else    
            }
        }
    })
};



/**
 * POST /forgot
 */


exports.forgotPost = function(req, res) {

    if (req.body.email) {
        var details = req.body.email;
        var detailsdata = {};


        userObj.findOne({
            email: details
        }, function(err, data) {
            console.log("inside findOne", JSON.stringify(data))
            if (data == null) {
                var response = {
                    "status": 'faliure',
                    "messageId": 401,
                    "message": "Email does not exist."
                };
                res.status(401).json(response);
            } else {
                var email_encrypt = commonjs.encrypt(data.email);
                var generatedText = commonjs.makeid();
                var resetUrl = "http://" + req.headers.host + "/#/" + "users/resetPost/" + data._id;
                console.log(resetUrl)
                console.log("aghsvfaghvfsJAXDVS", email_encrypt, "GENERTEAD ", generatedText, "resetUrl", resetUrl)

                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'bridgit871@gmail.com',
                        pass: 'bridgit8711'
                    }
                });
                var message = '<html><body style="background-color: #f2f2f2"><div style="width:90%; padding: 15px; background-color: #fff; box-shadow: 3px 3px #dddddd;"><div style="padding-top:10px; background-color: #f0f0f0; height: 100px"><h2>Bridgit<h2></div><div style="padding-top:10px">Hi,</div><div style="padding-top:30px">Your email has been used in a password reset request.</div><div style="padding-top:20px">If you did not initiate this request, then ignore this message.</div><div style="padding-top:20px">Copy the link below into your browser to reset your password.</div><div style="padding-top:30px"><a href="' + resetUrl + '">Reset Password</a></div><div style="padding-top:50px">Regards,<br>Bridgit</div></div></body></html>'

                transporter.sendMail({
                    from: 'bridgit871@gmail.com',

                    to: details,

                    subject: 'Password for Bridgit App',
                    html: message
                });


                res.status(200).send({
                    'resetUrl': resetUrl,
                    "status": 'success',
                    "messageId": 200,
                    "message": "Mail sent successfully."
                });
            }



        })
    } else {
        response = {
            "status": 'failure',
            "messageId": 401,
            "message": "Pass required fields."
        };
        res.status(401).json(response);
    }
}

/**
 * POST /reset
 */
exports.resetPost = function(req, res) {
    console.log("new passsssss", req.body)
    if (req.body._id != null) {
        if (req.body.password == req.body.confirm) {
            userObj.findOne({
                password: req.body.password,
                _id : req.body._id
            }, function(err, result) {
                if (err) {
                    outputJSON = {
                        'status': 'error',
                        'messageId': 400,
                        'message': "Password not updated, Try again later"
                    };
                    res.jsonp(outputJSON)
                } else {
                    console.log("fdfresult",result)
                    if (result) {
                        outputJSON = {
                            'status': 'error',
                            'messageId': 400,
                            'message': "Password already updated"
                        };
                        res.jsonp(outputJSON)
                    } else {
                        console.log("insode 1");
                        userObj.update({
                            _id: req.body._id
                        }, {
                            $set: {
                                "password": req.body.password
                            }
                        }, function(err, updatedresponse) {
                            if (err) {
                                outputJSON = {
                                    'status': 'error',
                                    'messageId': 400,
                                    'message': "Password not updated, Try again later"
                                };
                                res.jsonp(outputJSON)
                            } else {
                                console.log("updated responseeeeee", updatedresponse)
                                if (updatedresponse) {
                                    outputJSON = {
                                        'status': 'success',
                                        'messageId': 200,
                                        'message': "password updated successfully",
                                        "data": updatedresponse
                                    };
                                    res.jsonp(outputJSON)
                                } else {
                                    outputJSON = {
                                        'status': 'failure',
                                        'messageId': 400,
                                        'message': "password not updated,try again later"
                                    };
                                    res.jsonp(outputJSON)
                                }

                            }
                        })
                    }


                }
            })

        } else {
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': "Password and confirm password doesn't match",

            };
            res.jsonp(outputJSON)
        }
    } else {
        outputJSON = {
            'status': 'failure',
            'messageId': 400,
            'message': "please enter a reset password type",

        };
        res.jsonp(outputJSON)
    }


    /*} else {
        outputJSON = {
            'status': 'failure',
            'messageId': 400,
            'message': "session expired"
        };
        res.jsonp(outputJSON)
    }*/
}


