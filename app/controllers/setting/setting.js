var settingObj = require('./../../models/setting/setting.js');
var userObj = require('./../../models/users/users.js');
var constantObj = require('./../../../constants.js');

exports.saveSetting = function(req, res) {
	console.log(req.body);
	var saveObj = req.body;

	settingObj(saveObj).save(saveObj, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': "error in input" + err
			};
			res.status(200).jsonp(outputJSON)
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': "Save successfully",
				'data': data
			};
			res.status(200).jsonp(outputJSON)
		}
	})
}
exports.updateSetting = function(req, res) {
	console.log("req.body", req.body)
	console.log("req.params.id", req.params.id);
	var outputJSON = {};
	settingObj.update({
		_id: req.params.id
	}, {
		$set: req.body
	}, function(uErr, uData) {
		if (uErr) {
			outputJSON = {
				status: 400,
				message: uErr,
			}
			res.jsonp(outputJSON)
		} else {
			outputJSON = {
				status: 200,
				message: 'successfully updated',
			}
			res.jsonp(outputJSON)
		}
	})
}