 var dealObj = require('./../../models/dealStatus/dealStatus.js');
 var dealEditObj = require('./../../models/dealEdit/dealEdit.js');
 var mongoose = require('mongoose');
 var constantObj = require('./../../../constants.js');
 var common = require('./../common/common.js');
 var packageObj = require('./../../models/packages/package.js');
 var userObj = require('./../../models/users/users.js');
 var travellerObj = require('./../../models/traveller/traveller.js');
 let FCM = require('fcm-node');
 let serverKey = 'AIzaSyBd2WIKJkE3PDe101B4hERHjVnjx46-D6E';
 let fcm = new FCM(serverKey);

 exports.createdeals = function(req, res) {
     console.log("req.body", req.body);
     var outputJSON = {};
     dealObj(req.body).save(req.body, function(err, deal) {
         if (err) {
             outputJSON = {
                 status: 400,
                 message: err,
             }
             res.jsonp(outputJSON)
         } else {
             dealEditObj(req.body).save(req.body, function(err, dealedit) {
                 if (err) {
                     outputJSON = {
                         status: 400,
                         message: 'failure',
                     }
                     res.jsonp(outputJSON)
                 } else {
                    console.log("dealedit",dealedit)
                     outputJSON = {
                         status: 200,
                         message: 'successfully saved',
                     }
                     res.jsonp(outputJSON)
                     
                 }
             })
         }
     })
 }



 exports.updatedeals = function(req, res) {
     console.log("req.body", req.body)
     console.log("req.params.id", req.params.id);
     var outputJSON = {};
     dealObj.update({
         _id: req.params.id
     }, {
         $set: req.body
     }, function(uErr, uData) {
         if (uErr) {
             outputJSON = {
                 status: 400,
                 message: uErr,
             }
             res.jsonp(outputJSON)
         } else {
             dealEditObj(req.body).save(req.body, function(err, dealedit) {
                 if (err) {
                     outputJSON = {
                         status: 400,
                         message: 'failure',
                     }
                     res.jsonp(outputJSON)
                 } else {
                     outputJSON = {
                         status: 200,
                         message: 'successfully updated',
                     }
                     res.jsonp(outputJSON)
                 }
             })
         }
     })
 }

 exports.totalDeliveries = function(req, res) {
     var outputJSON = "";
     dealObj.count({
         is_deleted: false
     }, function(err, data) {
         if (err) {
             outputJSON = {
                 'status': 'failure',
                 'messageId': 203,
                 'message': constantObj.messages.errorRetreivingData
             };
         } else {
             outputJSON = {
                 'status': 'success',
                 'messageId': 200,
                 'message': constantObj.messages.successRetreivingData,
                 'data': data
             }
         }
         res.jsonp(outputJSON);
     });
 }