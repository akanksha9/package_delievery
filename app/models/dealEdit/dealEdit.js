var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var editdealSchema = new mongoose.Schema({

  package_id: {
    type: Schema.Types.ObjectId,ref: 'packages'
  },
  user_id: {
    type: Schema.Types.ObjectId,ref: 'users'
  },
  traveller_plan_id: {
    type: Schema.Types.ObjectId,ref: 'travellers'
  },
  budget: {
    type: Number
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

editdealSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};



var dealEditObj = mongoose.model('dealEdit', editdealSchema);
module.exports = dealEditObj;