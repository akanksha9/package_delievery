var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var reviewSchema = new mongoose.Schema({

  traveller_id: {
    type: Schema.Types.ObjectId,ref: 'travellers'
  },
  user_id: {
    type: Schema.Types.ObjectId,ref: 'users'
  },
  package_id: {
    type: Schema.Types.ObjectId,ref: 'packages'
  },
  review_rate: {
    type: Number
  },
  comment: {
    type: String                                                                                                                                            
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

reviewSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};



var reviewObj = mongoose.model('reviews', reviewSchema);
module.exports = reviewObj;