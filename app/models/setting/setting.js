var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var settingSchema = new Schema({
	user_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
	profileVisibility: {
		type: Boolean,
		default: true
	},
	distanceSelected: {
		type: Number,
		default: 0
	},
	newPackageNearBy: {
		type: Boolean,
		default: true
	},
	newTravellerNearBy: {
		type: Boolean,
		default: true
	},
	reqOnYourPackage: {
		type: Boolean,
		default: true
	},
	reqOnYourTraveller: {
		type: Boolean,
		default: true
	},
	notificationOnTracking: {
		type: Boolean,
		default: true
	},
	created_date: {
    type: Date,
    default: Date.now
  }
});

var settingObj = mongoose.model('settings', settingSchema);
module.exports = settingObj;