 var mongoose = require('mongoose');
 var uniqueValidator = require('mongoose-unique-validator');
 var Schema = mongoose.Schema;
 var userSchema = new mongoose.Schema({

   first_name: {
     type: String
   },
   last_name: {
     type: String
   },
   email: {
     type: String
   },
   password: {
     type: String
   },
   image: {
     type: String
   },
   gender: {
     type: String,
     default: "Female"
   },
   enable: {
     type: Boolean,
     default: true
   },
   phone_no: {
     type: String
   },
   address: {
     type: String
   },
   city: {
     type: String
   },
   country: {
     type: String
   },
   zipcode: {
     type: String
   },
   geo: {
     type: [Number],
     index: '2dsphere'
   },
   is_deleted: {
     type: Boolean,
     default: false
   },
   facebook_id: {
     type: String
   },
   loginType: {
     type: Number,
     default: 1
   }, // 1 simple,2 facebook
   created_date: {
     type: Date,
     default: Date.now
   },
   device_token: {
     type: String,
     default: "",
     require: true
   },
   device_type: {
     type: String,
     default: "",
     require: true
   },

   profileVisibility: {
     type: Boolean,
     default: true
   },
   distanceSelected: {
     type: Number,
     default: 100
   },
   newPackageNearBy: {
     type: Boolean,
     default: true
   },
   newTravellerNearBy: {
     type: Boolean,
     default: true
   },
   reqOnYourPackage: {
     type: Boolean,
     default: true
   },
   reqOnYourTraveller: {
     type: Boolean,
     default: true
   },
   notificationOnTracking: {
     type: Boolean,
     default: true
   },
   lastUpdatedLong: {
     type: Number,
     default: "70.708394"
   },
   lastUpdatedLat: {
     type: Number,
     default: "43.708394"
   },
   created_date: {
     type: Date,
     default: Date.now
   }

 });

 userSchema.statics.load = function(id, cb) {
   this.findOne({
       _id: id
     })
     .exec(cb);
 };

 userSchema.plugin(uniqueValidator, {
   message: "Username already exists."
 });

 var userObj = mongoose.model('users', userSchema);
 module.exports = userObj;