var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var userObj = require('./../users/users.js');
var dealStatus = require('../dealStatus/dealStatus.js');

var packageSchema = new mongoose.Schema({

	user_id: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	},
	deals: [{
		type: Schema.Types.ObjectId,
		ref: 'dealStatus'
	}],
	user_name: {
		type: String
	},
	email: {
		type: String
	},
	phone_no: {
		type: String,
		default: ""
	},
	package_name: {
		type: String
	},
	dimentions: {
		type: String
	},
	weight: {
		type: String
	},
	source: {
		type: String
	},
	destination: {
		type: String
	},
	last_comment: {
		type: String,
		default : ""
	},
	height: {
		type: String
	},
	width: {
		type: String
	},
	length: {
		type: String
	},
	enable: {
		type: Boolean,
		default: false
	},
	budget: {
		type: Number
	},
	is_req_to_traveller: {
		type: Boolean,
		default: false
	},
	is_req_to_package: {
		type: Boolean,
		default: false
	},
	startDate: {
		type: Date
	},
	endDate: {
		type: Date
	},
	description: {
		type: String
	},
	image: {
		type: String
	},
	quantity: {
		type: Number
	},
	picked_up: {
		type: Boolean
	},
	delivered_to_traveller: {
		type: Boolean
	},
	city: {
		type: String
	},
	country: {
		type: String
	},
	zipcode: {
		type: String
	},
	geo: {
		type: [Number],
		index: '2dsphere'
	},
	source_lat: {
		type: Number,
		default: "33.708394"
	},
	source_long: {
		type: Number,
		default: "76.702293"
	},
	localtion: {
		type: [],
		index: '2d'
	},
	destination_lat: {
		type: Number,
		default: "33.708394"
	},
	destination_long: {
		type: Number,
		default: "76.702293"
	},
	pick_lat: {
		type: Number,
		default: "33.708394"
	},
	pick_long: {
		type: Number,
		default: "76.702293"
	},
	address: {
		type: String
	},
	rating: {
		type: Number,
		default: 0
	},
	is_deleted: {
		type: Boolean,
		default: false
	},
	is_delivered: {
		type: Number,
		default: 0
			//1 for picked Up,2 for On the way,3 for successfully delivered,
			//4 for mark successfully delivered,5 for not successfully delivered.
	},
	count: {
		type: Number
	},
	created_date: {
		type: Date,
		default: Date.now
	}
});

packageSchema.statics.load = function(id, cb) {
	this.findOne({
			_id: id
		})
		.exec(cb);
};

packageSchema.plugin(uniqueValidator, {
	message: "package name already exists."
});

var packageObj = mongoose.model('packages', packageSchema);
module.exports = packageObj;