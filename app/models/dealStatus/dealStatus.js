var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var dealSchema = new mongoose.Schema({

  package_id: {
    type: Schema.Types.ObjectId,ref: 'packages'
  },
  user_id: {
    type: Schema.Types.ObjectId,ref: 'users'
  },
  traveller_plan_id: {
    type: Schema.Types.ObjectId,ref: 'traveller'
  },
  budget: {
    type: Number
  },
  status: {
    type: Number
  },//1 for Accept,2 for decline and 3 for edit
  is_deleted: {
    type: Boolean,
    default: false
  },
is_req_to_traveller:{
    type: Boolean,
    default: false
  },
is_req_to_package:{
    type: Boolean,
    default: false
  },
  
  created_date: {
    type: Date,
    default: Date.now
  }
});

dealSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};



var dealObj = mongoose.model('dealStatus', dealSchema);
module.exports = dealObj;