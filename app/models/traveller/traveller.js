var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var travellerSchema = new mongoose.Schema({

  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  deals: [{
    type: Schema.Types.ObjectId,
    ref: 'dealStatus'
  }],
  traveller_name: {
    type: String
  },
  email: {
    type: String
  },
  phone_no: {
    type: String,
    default: ""
  },
  image: {
    type: String,
    default:""
  },
  rating: {
    type: Number
  },
  source: {
    type: String
  },
  destination: {
    type: String
  },
  budget: {
    type: Number
  },
  startDate: {
    type: Date,
    default: Date.now
  },
  endDate: {
    type: Date,
    default: Date.now
  },
  enable: {
    type: Boolean,
    default: false
  },
  source_lat: {
    type: Number,
    default: "33.708394"
  },
  source_long: {
    type: Number,
    default: "76.702293"
  },
  localtion: {
    type: [],
    index: '2d'
  },
  destination_lat: {
    type: Number,
    default: "33.708394"
  },
  destination_long: {
    type: Number,
    default: "76.702293"
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  count: {
    type: Number
  },
  is_delivered: {
    type: Number,
    default: 0
  },
  last_comment: {
    type: String,
    default: ""
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

travellerSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};

travellerSchema.plugin(uniqueValidator, {
  message: "traveller name already exists."
});

var travellerObj = mongoose.model('traveller', travellerSchema);
module.exports = travellerObj;