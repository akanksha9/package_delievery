 module.exports = function(app, express, passport) {
 	var router = express.Router();
 	var userObj = require('./../app/controllers/users/users.js');



 	var router = express.Router();
 	router.post('/add', userObj.add);
 	router.post('/userlogin', passport.authenticate('userLogin'), userObj.userlogin);
 	router.post('/faceBookLogin', userObj.faceBookLogin);
 	router.post('/forgotPost', userObj.forgotPost);
 	router.post('/resetPost', userObj.resetPost);
 	router.post('/update_user_info', userObj.update_user_info);
 	router.post('/list', userObj.list);
 	router.post('/bulkUpdate', userObj.bulkUpdate);
 	router.post('/deleteUser', userObj.deleteUser);
 	router.get('/totalUser', userObj.totalUser);
 	router.put('/updateSetting/:id', userObj.updateSetting);

 	router.put('/updateLatLong/:id', userObj.updateLatLong);

 	app.use('/users', router);

 }