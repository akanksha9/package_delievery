module.exports = function(app, express, passport) {

	var router = express.Router();
	var settingObj = require('./../app/controllers/setting/setting.js');


	router.post('/saveSetting', settingObj.saveSetting);
	router.put('/saveSetting/:id', settingObj.updateSetting);
	

	app.use('/setting', router);



}