module.exports = function(app, express, passport) {

	var router = express.Router();
	var travellerObj = require('./../app/controllers/traveller/traveller.js');


	router.post('/addTravellerPlan', travellerObj.addTravellerPlan);
	router.post('/planList', travellerObj.planList);
	router.post('/myPlans', travellerObj.myPlans);
	router.post('/explore', travellerObj.explore);
	router.get('/totalTraveller', travellerObj.totalTraveller);
	router.post('/travellerList', travellerObj.travellerList);
	router.post('/deleteTraveller', travellerObj.deleteTraveller);
	router.post('/bulkUpdate', travellerObj.bulkUpdate);
	router.put('/tracking/:id', travellerObj.tracking) ;
	router.put('/updatePlans/:id', travellerObj.updatePlans) ;



	app.use('/traveller', router);


}