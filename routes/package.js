module.exports = function(app, express, passport) {

	var router = express.Router();
	var packageObj = require('./../app/controllers/package/package.js');

	router.post('/addpackage', packageObj.addpackage);
	router.post('/packageList', packageObj.packageList);
	router.post('/myPackages', packageObj.myPackages);
	router.get('/totalPackage', packageObj.totalPackage);
	router.post('/deletePackage', packageObj.deletePackage);
	router.post('/bulkUpdate', packageObj.bulkUpdate);
	router.post('/explore', packageObj.explore);
	router.put('/tracking/:id/:traveller', packageObj.tracking);
	router.put('/updatePackage/:id', packageObj.updatePackage);
	router.post('/parcelPosted', packageObj.parcelPosted);



	app.use('/package', router);



}