module.exports = function(app, express, passport) {

	var router = express.Router();
	var dealObj = require('./../app/controllers/dealStatus/dealStatus.js');


	router.post('/userdeals', dealObj.createdeals);
	router.put('/userdeals/:id', dealObj.updatedeals);

	router.get('/totalDeliveries', dealObj.totalDeliveries);



	app.use('/dealStatus', router);


}